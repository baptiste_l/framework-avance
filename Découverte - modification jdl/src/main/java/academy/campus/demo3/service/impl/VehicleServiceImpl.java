package academy.campus.demo3.service.impl;

import academy.campus.demo3.domain.Vehicle;
import academy.campus.demo3.repository.VehicleRepository;
import academy.campus.demo3.service.VehicleService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Vehicle}.
 */
@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

    private final Logger log = LoggerFactory.getLogger(VehicleServiceImpl.class);

    private final VehicleRepository vehicleRepository;

    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        log.debug("Request to save Vehicle : {}", vehicle);
        return vehicleRepository.save(vehicle);
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        log.debug("Request to save Vehicle : {}", vehicle);
        return vehicleRepository.save(vehicle);
    }

    @Override
    public Optional<Vehicle> partialUpdate(Vehicle vehicle) {
        log.debug("Request to partially update Vehicle : {}", vehicle);

        return vehicleRepository
            .findById(vehicle.getId())
            .map(existingVehicle -> {
                if (vehicle.getType() != null) {
                    existingVehicle.setType(vehicle.getType());
                }
                if (vehicle.getImmatriculation() != null) {
                    existingVehicle.setImmatriculation(vehicle.getImmatriculation());
                }
                if (vehicle.getLabel() != null) {
                    existingVehicle.setLabel(vehicle.getLabel());
                }
                if (vehicle.getKilometers() != null) {
                    existingVehicle.setKilometers(vehicle.getKilometers());
                }

                return existingVehicle;
            })
            .map(vehicleRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> findAll() {
        log.debug("Request to get all Vehicles");
        return vehicleRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Vehicle> findOne(Long id) {
        log.debug("Request to get Vehicle : {}", id);
        return vehicleRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Vehicle : {}", id);
        vehicleRepository.deleteById(id);
    }
}
