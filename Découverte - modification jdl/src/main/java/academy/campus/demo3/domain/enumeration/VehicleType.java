package academy.campus.demo3.domain.enumeration;

/**
 * The VehicleType enumeration.
 */
public enum VehicleType {
    CITADINE,
    BERLINE,
    BREAK,
    SUV,
    SPORTIVE,
}
