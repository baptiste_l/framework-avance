package academy.campus.demo3.domain;

import academy.campus.demo3.domain.enumeration.VehicleType;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Vehicle.
 */
@Entity
@Table(name = "vehicle")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private VehicleType type;

    @Pattern(regexp = "^([A-Z0-9]{2}\\-)([A-Z0-9]{3}\\-)[A-Z0-9]{2}$")
    @Column(name = "immatriculation")
    private String immatriculation;

    @Column(name = "label")
    private String label;

    @Column(name = "kilometers")
    private Long kilometers;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Vehicle id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleType getType() {
        return this.type;
    }

    public Vehicle type(VehicleType type) {
        this.setType(type);
        return this;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public String getImmatriculation() {
        return this.immatriculation;
    }

    public Vehicle immatriculation(String immatriculation) {
        this.setImmatriculation(immatriculation);
        return this;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getLabel() {
        return this.label;
    }

    public Vehicle label(String label) {
        this.setLabel(label);
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getKilometers() {
        return this.kilometers;
    }

    public Vehicle kilometers(Long kilometers) {
        this.setKilometers(kilometers);
        return this;
    }

    public void setKilometers(Long kilometers) {
        this.kilometers = kilometers;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vehicle)) {
            return false;
        }
        return id != null && id.equals(((Vehicle) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vehicle{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", immatriculation='" + getImmatriculation() + "'" +
            ", label='" + getLabel() + "'" +
            ", kilometers=" + getKilometers() +
            "}";
    }
}
