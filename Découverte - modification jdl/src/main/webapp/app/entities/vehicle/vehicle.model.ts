import { VehicleType } from 'app/entities/enumerations/vehicle-type.model';

export interface IVehicle {
  id?: number;
  type?: VehicleType | null;
  immatriculation?: string | null;
  label?: string | null;
  kilometers?: number | null;
}

export class Vehicle implements IVehicle {
  constructor(
    public id?: number,
    public type?: VehicleType | null,
    public immatriculation?: string | null,
    public label?: string | null,
    public kilometers?: number | null
  ) {}
}

export function getVehicleIdentifier(vehicle: IVehicle): number | undefined {
  return vehicle.id;
}
