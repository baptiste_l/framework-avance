import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IVehicle, Vehicle } from '../vehicle.model';
import { VehicleService } from '../service/vehicle.service';
import { VehicleType } from 'app/entities/enumerations/vehicle-type.model';

@Component({
  selector: 'jhi-vehicle-update',
  templateUrl: './vehicle-update.component.html',
})
export class VehicleUpdateComponent implements OnInit {
  isSaving = false;
  vehicleTypeValues = Object.keys(VehicleType);

  editForm = this.fb.group({
    id: [],
    type: [],
    immatriculation: [null, [Validators.pattern('^([A-Z0-9]{2}\\-)([A-Z0-9]{3}\\-)[A-Z0-9]{2}$')]],
    label: [],
    kilometers: [],
  });

  constructor(protected vehicleService: VehicleService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vehicle }) => {
      this.updateForm(vehicle);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vehicle = this.createFromForm();
    if (vehicle.id !== undefined) {
      this.subscribeToSaveResponse(this.vehicleService.update(vehicle));
    } else {
      this.subscribeToSaveResponse(this.vehicleService.create(vehicle));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVehicle>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(vehicle: IVehicle): void {
    this.editForm.patchValue({
      id: vehicle.id,
      type: vehicle.type,
      immatriculation: vehicle.immatriculation,
      label: vehicle.label,
      kilometers: vehicle.kilometers,
    });
  }

  protected createFromForm(): IVehicle {
    return {
      ...new Vehicle(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value,
      immatriculation: this.editForm.get(['immatriculation'])!.value,
      label: this.editForm.get(['label'])!.value,
      kilometers: this.editForm.get(['kilometers'])!.value,
    };
  }
}
