export enum VehicleType {
  CITADINE = 'CITADINE',

  BERLINE = 'BERLINE',

  BREAK = 'BREAK',

  SUV = 'SUV',

  SPORTIVE = 'SPORTIVE',
}
