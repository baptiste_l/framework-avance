import { IVehicle } from 'app/entities/vehicle/vehicle.model';
import { ITask } from 'app/entities/task/task.model';
import { IEmployee } from 'app/entities/employee/employee.model';

export interface IJob {
  id?: number;
  jobTitle?: string | null;
  minSalary?: number | null;
  maxSalary?: number | null;
  vehicle?: IVehicle | null;
  tasks?: ITask[] | null;
  employee?: IEmployee | null;
}

export class Job implements IJob {
  constructor(
    public id?: number,
    public jobTitle?: string | null,
    public minSalary?: number | null,
    public maxSalary?: number | null,
    public vehicle?: IVehicle | null,
    public tasks?: ITask[] | null,
    public employee?: IEmployee | null
  ) {}
}

export function getJobIdentifier(job: IJob): number | undefined {
  return job.id;
}
