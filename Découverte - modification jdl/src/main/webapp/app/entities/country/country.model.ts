import { IRegion } from 'app/entities/region/region.model';

export interface ICountry {
  id?: number;
  countryAbbr?: string | null;
  people?: number | null;
  countryName?: string | null;
  region?: IRegion | null;
}

export class Country implements ICountry {
  constructor(
    public id?: number,
    public countryAbbr?: string | null,
    public people?: number | null,
    public countryName?: string | null,
    public region?: IRegion | null
  ) {}
}

export function getCountryIdentifier(country: ICountry): number | undefined {
  return country.id;
}
