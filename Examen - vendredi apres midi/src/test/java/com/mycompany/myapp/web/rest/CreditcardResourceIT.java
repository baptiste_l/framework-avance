package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Creditcard;
import com.mycompany.myapp.repository.CreditcardRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CreditcardResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CreditcardResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_CARD_NUMBER = "0447 7873 1585 6293 ";
    private static final String UPDATED_CARD_NUMBER = "9803 9033 5110 1099 ";

    private static final String DEFAULT_EXPIRATION_DATE = "06/84";
    private static final String UPDATED_EXPIRATION_DATE = "89/19";

    private static final String DEFAULT_CRYPTOGRAM = "621";
    private static final String UPDATED_CRYPTOGRAM = "896";

    private static final String ENTITY_API_URL = "/api/creditcards";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CreditcardRepository creditcardRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCreditcardMockMvc;

    private Creditcard creditcard;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Creditcard createEntity(EntityManager em) {
        Creditcard creditcard = new Creditcard()
            .label(DEFAULT_LABEL)
            .cardNumber(DEFAULT_CARD_NUMBER)
            .expirationDate(DEFAULT_EXPIRATION_DATE)
            .cryptogram(DEFAULT_CRYPTOGRAM);
        return creditcard;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Creditcard createUpdatedEntity(EntityManager em) {
        Creditcard creditcard = new Creditcard()
            .label(UPDATED_LABEL)
            .cardNumber(UPDATED_CARD_NUMBER)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .cryptogram(UPDATED_CRYPTOGRAM);
        return creditcard;
    }

    @BeforeEach
    public void initTest() {
        creditcard = createEntity(em);
    }

    @Test
    @Transactional
    void createCreditcard() throws Exception {
        int databaseSizeBeforeCreate = creditcardRepository.findAll().size();
        // Create the Creditcard
        restCreditcardMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditcard)))
            .andExpect(status().isCreated());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeCreate + 1);
        Creditcard testCreditcard = creditcardList.get(creditcardList.size() - 1);
        assertThat(testCreditcard.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testCreditcard.getCardNumber()).isEqualTo(DEFAULT_CARD_NUMBER);
        assertThat(testCreditcard.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
        assertThat(testCreditcard.getCryptogram()).isEqualTo(DEFAULT_CRYPTOGRAM);
    }

    @Test
    @Transactional
    void createCreditcardWithExistingId() throws Exception {
        // Create the Creditcard with an existing ID
        creditcard.setId(1L);

        int databaseSizeBeforeCreate = creditcardRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreditcardMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditcard)))
            .andExpect(status().isBadRequest());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCreditcards() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        // Get all the creditcardList
        restCreditcardMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(creditcard.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE)))
            .andExpect(jsonPath("$.[*].cryptogram").value(hasItem(DEFAULT_CRYPTOGRAM)));
    }

    @Test
    @Transactional
    void getCreditcard() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        // Get the creditcard
        restCreditcardMockMvc
            .perform(get(ENTITY_API_URL_ID, creditcard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(creditcard.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.cardNumber").value(DEFAULT_CARD_NUMBER))
            .andExpect(jsonPath("$.expirationDate").value(DEFAULT_EXPIRATION_DATE))
            .andExpect(jsonPath("$.cryptogram").value(DEFAULT_CRYPTOGRAM));
    }

    @Test
    @Transactional
    void getNonExistingCreditcard() throws Exception {
        // Get the creditcard
        restCreditcardMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCreditcard() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();

        // Update the creditcard
        Creditcard updatedCreditcard = creditcardRepository.findById(creditcard.getId()).get();
        // Disconnect from session so that the updates on updatedCreditcard are not directly saved in db
        em.detach(updatedCreditcard);
        updatedCreditcard
            .label(UPDATED_LABEL)
            .cardNumber(UPDATED_CARD_NUMBER)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .cryptogram(UPDATED_CRYPTOGRAM);

        restCreditcardMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCreditcard.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCreditcard))
            )
            .andExpect(status().isOk());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
        Creditcard testCreditcard = creditcardList.get(creditcardList.size() - 1);
        assertThat(testCreditcard.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testCreditcard.getCardNumber()).isEqualTo(UPDATED_CARD_NUMBER);
        assertThat(testCreditcard.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testCreditcard.getCryptogram()).isEqualTo(UPDATED_CRYPTOGRAM);
    }

    @Test
    @Transactional
    void putNonExistingCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(
                put(ENTITY_API_URL_ID, creditcard.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditcard))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditcard))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditcard)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCreditcardWithPatch() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();

        // Update the creditcard using partial update
        Creditcard partialUpdatedCreditcard = new Creditcard();
        partialUpdatedCreditcard.setId(creditcard.getId());

        partialUpdatedCreditcard.cryptogram(UPDATED_CRYPTOGRAM);

        restCreditcardMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCreditcard.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreditcard))
            )
            .andExpect(status().isOk());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
        Creditcard testCreditcard = creditcardList.get(creditcardList.size() - 1);
        assertThat(testCreditcard.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testCreditcard.getCardNumber()).isEqualTo(DEFAULT_CARD_NUMBER);
        assertThat(testCreditcard.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
        assertThat(testCreditcard.getCryptogram()).isEqualTo(UPDATED_CRYPTOGRAM);
    }

    @Test
    @Transactional
    void fullUpdateCreditcardWithPatch() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();

        // Update the creditcard using partial update
        Creditcard partialUpdatedCreditcard = new Creditcard();
        partialUpdatedCreditcard.setId(creditcard.getId());

        partialUpdatedCreditcard
            .label(UPDATED_LABEL)
            .cardNumber(UPDATED_CARD_NUMBER)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .cryptogram(UPDATED_CRYPTOGRAM);

        restCreditcardMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCreditcard.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreditcard))
            )
            .andExpect(status().isOk());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
        Creditcard testCreditcard = creditcardList.get(creditcardList.size() - 1);
        assertThat(testCreditcard.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testCreditcard.getCardNumber()).isEqualTo(UPDATED_CARD_NUMBER);
        assertThat(testCreditcard.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testCreditcard.getCryptogram()).isEqualTo(UPDATED_CRYPTOGRAM);
    }

    @Test
    @Transactional
    void patchNonExistingCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, creditcard.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creditcard))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creditcard))
            )
            .andExpect(status().isBadRequest());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCreditcard() throws Exception {
        int databaseSizeBeforeUpdate = creditcardRepository.findAll().size();
        creditcard.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditcardMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(creditcard))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Creditcard in the database
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCreditcard() throws Exception {
        // Initialize the database
        creditcardRepository.saveAndFlush(creditcard);

        int databaseSizeBeforeDelete = creditcardRepository.findAll().size();

        // Delete the creditcard
        restCreditcardMockMvc
            .perform(delete(ENTITY_API_URL_ID, creditcard.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Creditcard> creditcardList = creditcardRepository.findAll();
        assertThat(creditcardList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
