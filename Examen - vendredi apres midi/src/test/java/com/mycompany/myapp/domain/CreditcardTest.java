package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CreditcardTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Creditcard.class);
        Creditcard creditcard1 = new Creditcard();
        creditcard1.setId(1L);
        Creditcard creditcard2 = new Creditcard();
        creditcard2.setId(creditcard1.getId());
        assertThat(creditcard1).isEqualTo(creditcard2);
        creditcard2.setId(2L);
        assertThat(creditcard1).isNotEqualTo(creditcard2);
        creditcard1.setId(null);
        assertThat(creditcard1).isNotEqualTo(creditcard2);
    }
}
