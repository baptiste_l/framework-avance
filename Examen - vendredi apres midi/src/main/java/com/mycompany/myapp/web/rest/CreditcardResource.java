package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Creditcard;
import com.mycompany.myapp.repository.CreditcardRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Creditcard}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CreditcardResource {

    private final Logger log = LoggerFactory.getLogger(CreditcardResource.class);

    private static final String ENTITY_NAME = "creditcard";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CreditcardRepository creditcardRepository;

    public CreditcardResource(CreditcardRepository creditcardRepository) {
        this.creditcardRepository = creditcardRepository;
    }

    /**
     * {@code POST  /creditcards} : Create a new creditcard.
     *
     * @param creditcard the creditcard to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new creditcard, or with status {@code 400 (Bad Request)} if the creditcard has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/creditcards")
    public ResponseEntity<Creditcard> createCreditcard(@Valid @RequestBody Creditcard creditcard) throws URISyntaxException {
        log.debug("REST request to save Creditcard : {}", creditcard);
        if (creditcard.getId() != null) {
            throw new BadRequestAlertException("A new creditcard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Creditcard result = creditcardRepository.save(creditcard);
        return ResponseEntity
            .created(new URI("/api/creditcards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /creditcards/:id} : Updates an existing creditcard.
     *
     * @param id the id of the creditcard to save.
     * @param creditcard the creditcard to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creditcard,
     * or with status {@code 400 (Bad Request)} if the creditcard is not valid,
     * or with status {@code 500 (Internal Server Error)} if the creditcard couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/creditcards/{id}")
    public ResponseEntity<Creditcard> updateCreditcard(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Creditcard creditcard
    ) throws URISyntaxException {
        log.debug("REST request to update Creditcard : {}, {}", id, creditcard);
        if (creditcard.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creditcard.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creditcardRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Creditcard result = creditcardRepository.save(creditcard);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, creditcard.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /creditcards/:id} : Partial updates given fields of an existing creditcard, field will ignore if it is null
     *
     * @param id the id of the creditcard to save.
     * @param creditcard the creditcard to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creditcard,
     * or with status {@code 400 (Bad Request)} if the creditcard is not valid,
     * or with status {@code 404 (Not Found)} if the creditcard is not found,
     * or with status {@code 500 (Internal Server Error)} if the creditcard couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/creditcards/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Creditcard> partialUpdateCreditcard(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Creditcard creditcard
    ) throws URISyntaxException {
        log.debug("REST request to partial update Creditcard partially : {}, {}", id, creditcard);
        if (creditcard.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creditcard.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creditcardRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Creditcard> result = creditcardRepository
            .findById(creditcard.getId())
            .map(existingCreditcard -> {
                if (creditcard.getLabel() != null) {
                    existingCreditcard.setLabel(creditcard.getLabel());
                }
                if (creditcard.getCardNumber() != null) {
                    existingCreditcard.setCardNumber(creditcard.getCardNumber());
                }
                if (creditcard.getExpirationDate() != null) {
                    existingCreditcard.setExpirationDate(creditcard.getExpirationDate());
                }
                if (creditcard.getCryptogram() != null) {
                    existingCreditcard.setCryptogram(creditcard.getCryptogram());
                }

                return existingCreditcard;
            })
            .map(creditcardRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, creditcard.getId().toString())
        );
    }

    /**
     * {@code GET  /creditcards} : get all the creditcards.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of creditcards in body.
     */
    @GetMapping("/creditcards")
    public List<Creditcard> getAllCreditcards() {
        log.debug("REST request to get all Creditcards");
        return creditcardRepository.findAll();
    }

    /**
     * {@code GET  /creditcards/:id} : get the "id" creditcard.
     *
     * @param id the id of the creditcard to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the creditcard, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/creditcards/{id}")
    public ResponseEntity<Creditcard> getCreditcard(@PathVariable Long id) {
        log.debug("REST request to get Creditcard : {}", id);
        Optional<Creditcard> creditcard = creditcardRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(creditcard);
    }

    /**
     * {@code DELETE  /creditcards/:id} : delete the "id" creditcard.
     *
     * @param id the id of the creditcard to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/creditcards/{id}")
    public ResponseEntity<Void> deleteCreditcard(@PathVariable Long id) {
        log.debug("REST request to delete Creditcard : {}", id);
        creditcardRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
