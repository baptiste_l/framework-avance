package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Creditcard;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Creditcard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CreditcardRepository extends JpaRepository<Creditcard, Long> {}
