/**
 * View Models used by Spring MVC REST controllers.
 */
package lefeuvre.baptiste.exam.web.rest.vm;
