import dayjs from 'dayjs/esm';

export interface IOrder {
  id?: number;
  dateOrder?: dayjs.Dayjs | null;
}

export class Order implements IOrder {
  constructor(public id?: number, public dateOrder?: dayjs.Dayjs | null) {}
}

export function getOrderIdentifier(order: IOrder): number | undefined {
  return order.id;
}
