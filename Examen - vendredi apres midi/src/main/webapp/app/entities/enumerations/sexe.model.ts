export enum Sexe {
  MASCULIN = 'MASCULIN',

  FEMININ = 'FEMININ',

  AUTRE = 'AUTRE',
}
