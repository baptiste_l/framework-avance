import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CreditcardDetailComponent } from './creditcard-detail.component';

describe('Creditcard Management Detail Component', () => {
  let comp: CreditcardDetailComponent;
  let fixture: ComponentFixture<CreditcardDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreditcardDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ creditcard: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CreditcardDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CreditcardDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load creditcard on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.creditcard).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
