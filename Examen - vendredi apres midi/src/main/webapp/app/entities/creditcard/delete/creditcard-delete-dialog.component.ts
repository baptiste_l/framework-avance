import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICreditcard } from '../creditcard.model';
import { CreditcardService } from '../service/creditcard.service';

@Component({
  templateUrl: './creditcard-delete-dialog.component.html',
})
export class CreditcardDeleteDialogComponent {
  creditcard?: ICreditcard;

  constructor(protected creditcardService: CreditcardService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.creditcardService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
