import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CreditcardComponent } from '../list/creditcard.component';
import { CreditcardDetailComponent } from '../detail/creditcard-detail.component';
import { CreditcardUpdateComponent } from '../update/creditcard-update.component';
import { CreditcardRoutingResolveService } from './creditcard-routing-resolve.service';

const creditcardRoute: Routes = [
  {
    path: '',
    component: CreditcardComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CreditcardDetailComponent,
    resolve: {
      creditcard: CreditcardRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CreditcardUpdateComponent,
    resolve: {
      creditcard: CreditcardRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CreditcardUpdateComponent,
    resolve: {
      creditcard: CreditcardRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(creditcardRoute)],
  exports: [RouterModule],
})
export class CreditcardRoutingModule {}
