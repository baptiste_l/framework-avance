import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICreditcard, Creditcard } from '../creditcard.model';
import { CreditcardService } from '../service/creditcard.service';

@Injectable({ providedIn: 'root' })
export class CreditcardRoutingResolveService implements Resolve<ICreditcard> {
  constructor(protected service: CreditcardService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICreditcard> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((creditcard: HttpResponse<Creditcard>) => {
          if (creditcard.body) {
            return of(creditcard.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Creditcard());
  }
}
