import { ICustomer } from 'app/entities/customer/customer.model';

export interface ICreditcard {
  id?: number;
  label?: string | null;
  cardNumber?: string | null;
  expirationDate?: string | null;
  cryptogram?: string | null;
  customer?: ICustomer | null;
}

export class Creditcard implements ICreditcard {
  constructor(
    public id?: number,
    public label?: string | null,
    public cardNumber?: string | null,
    public expirationDate?: string | null,
    public cryptogram?: string | null,
    public customer?: ICustomer | null
  ) {}
}

export function getCreditcardIdentifier(creditcard: ICreditcard): number | undefined {
  return creditcard.id;
}
