import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICreditcard, getCreditcardIdentifier } from '../creditcard.model';

export type EntityResponseType = HttpResponse<ICreditcard>;
export type EntityArrayResponseType = HttpResponse<ICreditcard[]>;

@Injectable({ providedIn: 'root' })
export class CreditcardService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/creditcards');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(creditcard: ICreditcard): Observable<EntityResponseType> {
    return this.http.post<ICreditcard>(this.resourceUrl, creditcard, { observe: 'response' });
  }

  update(creditcard: ICreditcard): Observable<EntityResponseType> {
    return this.http.put<ICreditcard>(`${this.resourceUrl}/${getCreditcardIdentifier(creditcard) as number}`, creditcard, {
      observe: 'response',
    });
  }

  partialUpdate(creditcard: ICreditcard): Observable<EntityResponseType> {
    return this.http.patch<ICreditcard>(`${this.resourceUrl}/${getCreditcardIdentifier(creditcard) as number}`, creditcard, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICreditcard>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICreditcard[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCreditcardToCollectionIfMissing(
    creditcardCollection: ICreditcard[],
    ...creditcardsToCheck: (ICreditcard | null | undefined)[]
  ): ICreditcard[] {
    const creditcards: ICreditcard[] = creditcardsToCheck.filter(isPresent);
    if (creditcards.length > 0) {
      const creditcardCollectionIdentifiers = creditcardCollection.map(creditcardItem => getCreditcardIdentifier(creditcardItem)!);
      const creditcardsToAdd = creditcards.filter(creditcardItem => {
        const creditcardIdentifier = getCreditcardIdentifier(creditcardItem);
        if (creditcardIdentifier == null || creditcardCollectionIdentifiers.includes(creditcardIdentifier)) {
          return false;
        }
        creditcardCollectionIdentifiers.push(creditcardIdentifier);
        return true;
      });
      return [...creditcardsToAdd, ...creditcardCollection];
    }
    return creditcardCollection;
  }
}
