import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICreditcard, Creditcard } from '../creditcard.model';

import { CreditcardService } from './creditcard.service';

describe('Creditcard Service', () => {
  let service: CreditcardService;
  let httpMock: HttpTestingController;
  let elemDefault: ICreditcard;
  let expectedResult: ICreditcard | ICreditcard[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CreditcardService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      label: 'AAAAAAA',
      cardNumber: 'AAAAAAA',
      expirationDate: 'AAAAAAA',
      cryptogram: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Creditcard', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Creditcard()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Creditcard', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          label: 'BBBBBB',
          cardNumber: 'BBBBBB',
          expirationDate: 'BBBBBB',
          cryptogram: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Creditcard', () => {
      const patchObject = Object.assign(
        {
          label: 'BBBBBB',
          expirationDate: 'BBBBBB',
          cryptogram: 'BBBBBB',
        },
        new Creditcard()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Creditcard', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          label: 'BBBBBB',
          cardNumber: 'BBBBBB',
          expirationDate: 'BBBBBB',
          cryptogram: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Creditcard', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCreditcardToCollectionIfMissing', () => {
      it('should add a Creditcard to an empty array', () => {
        const creditcard: ICreditcard = { id: 123 };
        expectedResult = service.addCreditcardToCollectionIfMissing([], creditcard);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(creditcard);
      });

      it('should not add a Creditcard to an array that contains it', () => {
        const creditcard: ICreditcard = { id: 123 };
        const creditcardCollection: ICreditcard[] = [
          {
            ...creditcard,
          },
          { id: 456 },
        ];
        expectedResult = service.addCreditcardToCollectionIfMissing(creditcardCollection, creditcard);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Creditcard to an array that doesn't contain it", () => {
        const creditcard: ICreditcard = { id: 123 };
        const creditcardCollection: ICreditcard[] = [{ id: 456 }];
        expectedResult = service.addCreditcardToCollectionIfMissing(creditcardCollection, creditcard);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(creditcard);
      });

      it('should add only unique Creditcard to an array', () => {
        const creditcardArray: ICreditcard[] = [{ id: 123 }, { id: 456 }, { id: 78681 }];
        const creditcardCollection: ICreditcard[] = [{ id: 123 }];
        expectedResult = service.addCreditcardToCollectionIfMissing(creditcardCollection, ...creditcardArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const creditcard: ICreditcard = { id: 123 };
        const creditcard2: ICreditcard = { id: 456 };
        expectedResult = service.addCreditcardToCollectionIfMissing([], creditcard, creditcard2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(creditcard);
        expect(expectedResult).toContain(creditcard2);
      });

      it('should accept null and undefined values', () => {
        const creditcard: ICreditcard = { id: 123 };
        expectedResult = service.addCreditcardToCollectionIfMissing([], null, creditcard, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(creditcard);
      });

      it('should return initial array if no Creditcard is added', () => {
        const creditcardCollection: ICreditcard[] = [{ id: 123 }];
        expectedResult = service.addCreditcardToCollectionIfMissing(creditcardCollection, undefined, null);
        expect(expectedResult).toEqual(creditcardCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
