import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CreditcardComponent } from './list/creditcard.component';
import { CreditcardDetailComponent } from './detail/creditcard-detail.component';
import { CreditcardUpdateComponent } from './update/creditcard-update.component';
import { CreditcardDeleteDialogComponent } from './delete/creditcard-delete-dialog.component';
import { CreditcardRoutingModule } from './route/creditcard-routing.module';

@NgModule({
  imports: [SharedModule, CreditcardRoutingModule],
  declarations: [CreditcardComponent, CreditcardDetailComponent, CreditcardUpdateComponent, CreditcardDeleteDialogComponent],
  entryComponents: [CreditcardDeleteDialogComponent],
})
export class CreditcardModule {}
