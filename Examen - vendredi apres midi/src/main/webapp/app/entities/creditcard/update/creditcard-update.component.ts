import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICreditcard, Creditcard } from '../creditcard.model';
import { CreditcardService } from '../service/creditcard.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

@Component({
  selector: 'jhi-creditcard-update',
  templateUrl: './creditcard-update.component.html',
})
export class CreditcardUpdateComponent implements OnInit {
  isSaving = false;

  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    label: [],
    cardNumber: [null, [Validators.pattern('^([0-9]{4}\\s){4}$')]],
    expirationDate: [null, [Validators.pattern('^[0-9]{2}\\/[0-9]{2}$')]],
    cryptogram: [null, [Validators.pattern('^[0-9]{3}$')]],
    customer: [],
  });

  constructor(
    protected creditcardService: CreditcardService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ creditcard }) => {
      this.updateForm(creditcard);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const creditcard = this.createFromForm();
    if (creditcard.id !== undefined) {
      this.subscribeToSaveResponse(this.creditcardService.update(creditcard));
    } else {
      this.subscribeToSaveResponse(this.creditcardService.create(creditcard));
    }
  }

  trackCustomerById(_index: number, item: ICustomer): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICreditcard>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(creditcard: ICreditcard): void {
    this.editForm.patchValue({
      id: creditcard.id,
      label: creditcard.label,
      cardNumber: creditcard.cardNumber,
      expirationDate: creditcard.expirationDate,
      cryptogram: creditcard.cryptogram,
      customer: creditcard.customer,
    });

    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      creditcard.customer
    );
  }

  protected loadRelationshipsOptions(): void {
    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): ICreditcard {
    return {
      ...new Creditcard(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      cardNumber: this.editForm.get(['cardNumber'])!.value,
      expirationDate: this.editForm.get(['expirationDate'])!.value,
      cryptogram: this.editForm.get(['cryptogram'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }
}
