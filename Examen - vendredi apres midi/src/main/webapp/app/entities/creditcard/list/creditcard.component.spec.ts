import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CreditcardService } from '../service/creditcard.service';

import { CreditcardComponent } from './creditcard.component';

describe('Creditcard Management Component', () => {
  let comp: CreditcardComponent;
  let fixture: ComponentFixture<CreditcardComponent>;
  let service: CreditcardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CreditcardComponent],
    })
      .overrideTemplate(CreditcardComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CreditcardComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CreditcardService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.creditcards?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
