import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICreditcard } from '../creditcard.model';
import { CreditcardService } from '../service/creditcard.service';
import { CreditcardDeleteDialogComponent } from '../delete/creditcard-delete-dialog.component';

@Component({
  selector: 'jhi-creditcard',
  templateUrl: './creditcard.component.html',
})
export class CreditcardComponent implements OnInit {
  creditcards?: ICreditcard[];
  isLoading = false;

  constructor(protected creditcardService: CreditcardService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.creditcardService.query().subscribe({
      next: (res: HttpResponse<ICreditcard[]>) => {
        this.isLoading = false;
        this.creditcards = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: ICreditcard): number {
    return item.id!;
  }

  delete(creditcard: ICreditcard): void {
    const modalRef = this.modalService.open(CreditcardDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.creditcard = creditcard;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
