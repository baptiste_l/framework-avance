import { IAddress } from 'app/entities/address/address.model';
import { ICreditcard } from 'app/entities/creditcard/creditcard.model';
import { IOrder } from 'app/entities/order/order.model';
import { Sexe } from 'app/entities/enumerations/sexe.model';

export interface ICustomer {
  id?: number;
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  telephone?: string | null;
  sexe?: Sexe | null;
  addresses?: IAddress[] | null;
  creditCards?: ICreditcard[] | null;
  order?: IOrder | null;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public firstName?: string | null,
    public lastName?: string | null,
    public email?: string | null,
    public telephone?: string | null,
    public sexe?: Sexe | null,
    public addresses?: IAddress[] | null,
    public creditCards?: ICreditcard[] | null,
    public order?: IOrder | null
  ) {}
}

export function getCustomerIdentifier(customer: ICustomer): number | undefined {
  return customer.id;
}
