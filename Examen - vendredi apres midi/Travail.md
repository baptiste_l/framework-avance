# Examen

## Finalités de l'application
### JDL
L'application va gérer un stock contenant les entités suivantes:
```
entity Category {
    description String required,
    sortOrder Integer,
    dateAdded LocalDate,
    dateModified LocalDate,
    status CategoryStatus
}

entity Product {
    title String required,
    keywords String,
    description String,
    rating Integer,
    dateAdded LocalDate,
    dateModified LocalDate
}

entity Customer {
    firstName String,
    lastName String,
    email String,
    telephone String
}

entity Address {
    address1 String,
    address2 String,
    city String,
    postcode String required maxlength(10),
    country String required maxlength(2)
}
```

### Ajouts au JDL
- Modification de l'entité **Customer** (pattern pour le téléphone, adresse email)
- Modification de l'entité **Customer** (Ajout d'un attribut sexe (ENUM))
- Ajout une entité **credit_card** associé à un client.
- Ajout d'une entité **order**, qui associe un client, plusieurs produits, ainsi qu'une addresse de livraison

### Ajout Angular
- Ajout d'une page web statique (aide)

## Installation 

Tout d'abord, vous devez clôner le projet via la commande suivante:
```bash
git clone -b Examen https://gitlab.com/baptiste_l/jhipster/
```

ensuite, vous devez installer les nodes modules requis
```bash
cd jhipster/VENDREDI_APRES_MIDI
npm install
```
## Génération de l'applciation via la commande Jhipster
```
? Which *type* of application would you like to create? Monolithic application
? What is the base name of your application? Examen
? Do you want to make it reactive with Spring WebFlux? No
? What is your default Java package name? lefeuvre.baptiste.exam
? Which *type* of authentication would you like to use? JWT authentication 
? Which *type* of database would you like to use? SQL (H2, PostgreSQL, MySQL, MariaDB, Oracle, MSSQL)
? Which *production* database would you like to use? PostgreSQL
? Which *development* database would you like to use? PostgreSQL
? Which cache do you want to use? (Spring cache abstraction) Ehcache (local cache, for a single node)
? Do you want to use Hibernate 2nd level cache? No
? Would you like to use Maven or Gradle for building the backend? Maven
? Do you want to use the JHipster Registry to configure, monitor and scale your application? No
? Which other technologies would you like to use? 
? Which *Framework* would you like to use for the client? Angular
? Do you want to generate the admin UI? Yes
? Would you like to use a Bootswatch theme (https://bootswatch.com/)? Default JHipster
? Would you like to enable internationalization support? Yes
? Please choose the native language of the application English
? Please choose additional languages to install 
? Besides JUnit and Jest, which testing frameworks would you like to use? 
? Would you like to install other generators from the JHipster Marketplace? No
```

## Ajout de la langue Français
Nous allons exécuter la commande suivante afin d'ajouter une langue à notre projet 
```bash
jhipster languages
```
Nous écrasons ensuite les fichiers précédents avec la nouvelle configuration.

## Ajout de la base de donnée Postgres (Docker)
Afin d'utiliser la BDD postgresSQL, il nous faut un conteneur docker. Lors de la créaction du projet Jhipster, un dossier **src/main/docker** a été créé, regroupant plusieurs conteneurs docker disponibles pour l'utilisation.
Dans notre cas, nous allons utiliser **postgrssql.yml**

Nous allons vérifier le port dans le ficheir **postgresql.yml**, dans notre cas, 5432.
Ensuite, nous allons vérifier quel'URL de la base de donnée est bien renseignée dans le fichier **src/main/resources/config/application-dev.yml**
Notre conteneur est accessible via localhost:5432, et la base de donnée s'appelle Examen.

```
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://localhost:5432/jhipster
    username: jhipster
    password: jhipster
    hikari:
      poolName: Hikari
      auto-commit: false
```
Nous allons ensuite démarrer notre conteneur via la commande suivante: 
````
docker-compose -f src/main/docker/postgresql.yml up
````

## Ajout du JDL
Le JDL utilisé peut être trouvé à cette addresse (*https://raw.githubusercontent.com/jhipster/jdl-samples/main/simple-online-shop.jh*).

### Téléchargement du JDL
```bash
wget https://raw.githubusercontent.com/jhipster/jdl-samples/main/simple-online-shop.jh -O jhipster-jdl.jdl
```

### Exécution du JDL
```bash
jhipster jdl jhipster-jdl.jdl
```

Un conflit survient, et Jhipster nosu demande si nous voulons écraser el fichier précédent, nous répondons Oui pour tous. (a)

```bash
 conflict package.json
? Overwrite package.json? overwrite this and all others
```

### Exécution de l'application
Nous allons maint'enant voir le résultat en exécutant la commande 
```bash
./mvnw
```
Nous obtenons dans la console les URL de connexion, et nous obtenons noter front avec les 4 entités 
```bash
----------------------------------------------------------
        Application 'jhipster' is running! Access URLs:
        Local:          http://localhost:8080/
        External:       http://192.168.1.22:8080/
        Profile(s):     [dev, api-docs]
----------------------------------------------------------
```

## Modification de l'entité Customer (pattern pour le téléphone, adresse email)
Nous allons changer l'entité customer du JDL afin d'y ajouter les informatiosn suivantes:
- Pattern pour le numéro de téléphone (+33) 01-02-03-04-05)
- Pattern pour l'adresse mail (forcer un @ dans l'email)
- Ajout d'un attribut sexe (qui peut être **MASCULIN**, **FEMININ**, **AUTRE** ou **NON RENSEIGNE**)


```bash
entity Customer {
    firstName String,
    lastName String,
    email String pattern(/^(.+)@(.+)$/),
    telephone String pattern(/^\((\+[0-9]{2,3})\)\s([0-9]{2}\-){4}[0-9]{2}$/)
    sexe sexeEnum
}

enum sexeEnum {
    MARCULIN, FEMININ, AUTRE
}

```

## Modification de l'entité Customer (pattern pour le téléphone, adresse email)
Nous allons changer l'entité customer du JDL afin d'y ajouter 
```bash
entity Customer {
    firstName String,
    lastName String,
    email String pattern(/^(.+)@(.+)$/),
    telephone String pattern(/^\(\+[0-9]{3}\)([0-9]{2}\-){4}[0-9]{2}$/)
}
```

## Ajout d'une page angular
Nous allons commencer par créer un composant **help** via la commande suivante: 
```bash
ng g c help
```
Nous allons ensuite modifier le fichier **src/main/app/app-routing.modiule.ts** afin de rajouter la route de notre composant, pour que la route /help pointe vers notre composant.
```bash
@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'help',
          component: HelpComponent,
        },
    ...
```