# Erreurs rencontrés

## Ajout d'un attribut peopleCount dans country.
Erreur suivante est relevée: 
```
Column "COUNTRY0_.COUNTRY_PEOPLE_COUNT" not found; SQL statement:
select country0_.id as id1_0_, country0_.country_name as country_2_0_, country0_.country_people_count as country_3_0_, country0_.region_id as region_i4_0_ from country country0_ [42122-200]
```

### Tentative de correction:
``` 
./mvnw liquibase:diff
```
Ne fonctionne pas, url de la base de donéne non trouvée.