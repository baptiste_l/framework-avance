# Framework avacé

## Liste des dossiers
- Découverte - modification jdl

        Correspond aux modifications des entités via les commandes jhipster
- Découverte - modification manuelle

        Correspond aux modification apportés aux entitiés à la main.
- Examen - vendredi apres midi

        Correspond au TP noté de Vendredi après midi
        Vous trouverez un **travail.md** à l'intérieur de ce fichier, détaillant la procédure ainsi que mes notes.
